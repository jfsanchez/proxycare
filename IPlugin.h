/**
* ProxyCare a CNTLM proxy GUI.
* Copyright (C) 2016 Jorge Fernández Sánchez <jfsanchez.email@gmail.com>
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
**/

#ifndef IPLUGIN_H
#define IPLUGIN_H

#include <QWidget>
#include <QString>
#include <QIcon>

class IPlugin
{
public:
	virtual ~IPlugin(){}
	virtual QString name() = 0;
	virtual QWidget* widget() = 0;
	virtual QString tabToolTip() = 0;
	virtual QIcon tabIcon() = 0;
};

#define IPlugin_idd "com.jfsanchez.proxycare.iplugin"

Q_DECLARE_INTERFACE(IPlugin, IPlugin_idd)


#endif // IPLUGIN_H
