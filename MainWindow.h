/**
* ProxyCare a CNTLM proxy GUI.
* Copyright (C) 2016 Jorge Fernández Sánchez <jfsanchez.email@gmail.com>
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
**/

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QSystemTrayIcon>
#include <QDomDocument>
#include <QDialog>
#include <iostream>
#include <QList>
#include <QProcess>
#include <QCloseEvent>
#include <QMessageBox>
#include <QMenu>
#include <QFile>
#include <QTextStream>
#include <QDir>
#include "ui_MainWindow.h"
#include <QTimer>
#include <BigAES.h>
#include <QList>

#include "QuotaManager.h"
class IPlugin;

class MainWindow : public QDialog, private Ui_MainWindow
{
	Q_OBJECT

public:
	explicit MainWindow(QWidget *parent = 0);
	~MainWindow();
	void setVisible(bool visible);
	
public slots:
	void showMessage(QString title, QString message, int time);
	void quit();
	
protected:
	void closeEvent(QCloseEvent *event);

private slots:
	void hide();
	void iconActivated(QSystemTrayIcon::ActivationReason reason);
	void messageClicked();
	void readConfiguration();
	void acceptPushButtonClicked();
	void saveConfiguration();
	void standardOutput();
	void standardError();
	void all();
	void terminateProcess();
	void processFinished();
	void processStarted();
	void processError(QProcess::ProcessError error);
	void uiReadyToConnect();
	void uiConnected();
	void onQuotaUpdated(QString user, QString quote_used, QString quote_assigned, QString nav_level);
	void onCheckBox1Changed(int);
	void onCheckBox2Changed(int);
	void onSpinValueChanged(int);
	void changeConnectionStatus();

private:
	QAction *minimizeAction;
	QAction *restoreAction;
	QAction *quitAction;
	QSystemTrayIcon *trayIcon;
	QMenu *trayIconMenu;
	QString configPath;
	QString cntlmConfigPath;
	QProcess *process;
	bool hidden;
	QString workPath;
	QString cntlmPath;
	bool start , temp_message, m_connected;
	QuotaManager *qmanager;
	QTimer *timer;
	BigAES *aes;
	QByteArray ppassword;
	QList<IPlugin *> plugins;
	
	
	//functions
	void createIconGroupBox();
	void createMessageGroupBox();
	void createActions();
	void createTrayIcon();
	void goConnected();
	void goDisconnected();
	void encrypt(QString fileName, QByteArray data);
	QByteArray decrypt(QString fileName);
	int loadPlugins();
	void unloadPlugin(IPlugin *plugin);
	void populateTabs(QObject *plugin, int *count);

signals:
	void readyToConnect();
	void connected();
};

#endif // MAINWINDOW_H
