/**
* ProxyCare a CNTLM proxy GUI.
* Copyright (C) 2016 Jorge Fernández Sánchez <jfsanchez.email@gmail.com>
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
**/

#include "MainWindow.h"
#include <QDebug>
#include <QProgressBar>
#include <QThreadPool>
#include <QFile>
#include <QDir>
#include  <QPluginLoader>
#include <QVBoxLayout>
#include "IPlugin.h"


MainWindow::MainWindow(QWidget *parent):
QDialog(parent)
{
	setupUi(this);
	loadPlugins();

	aes = new BigAES();
	QString token = "`1234567890-=~!@#$%^&*()_+qwertyuiop[]}proxycare1.0{POIUYTREWQ|asdfghjkl;':LKJHGFDSAzxcvbnm,./?><MNBVCXZ";
	ppassword = QCryptographicHash::hash(token.toUtf8(), QCryptographicHash::Md5).toHex();

	cntlmPath = "/usr/lib/proxycare/cntlm";

	createActions();
	createTrayIcon();
	process = new QProcess();
	connect(trayIcon, SIGNAL(messageClicked()), this, SLOT(messageClicked()));
	connect(trayIcon, SIGNAL(activated(QSystemTrayIcon::ActivationReason)),
	        this, SLOT(iconActivated(QSystemTrayIcon::ActivationReason)));
	//connect(optionsPushButton,SIGNAL(clicked()),this,SLOT(showOptionsDialog()));
	connect(process, SIGNAL(readyReadStandardOutput()), this, SLOT(standardOutput()));
	connect(process, SIGNAL(readyReadStandardError()), this, SLOT(standardError()));
	connect(process, SIGNAL(readyRead()), this, SLOT(all()));
	connect(process, SIGNAL(started()), this, SLOT(processStarted()));
	connect(process, SIGNAL(finished(int)), this, SLOT(processFinished()));
	connect(process, SIGNAL(error(QProcess::ProcessError)), this, SLOT(processError(QProcess::ProcessError)));
	connect(cancellPushButton, SIGNAL(clicked()), this, SLOT(terminateProcess()));
	connect(acceptPushButton, SIGNAL(clicked()), this, SLOT(acceptPushButtonClicked()));
	connect(this, SIGNAL(readyToConnect()), this, SLOT(uiReadyToConnect()));
	connect(this, SIGNAL(connected()), this, SLOT(uiConnected()));
	trayIcon->show();
	setFixedSize(270, 305);

	m_connected = false;
	timer = new QTimer(this);
	qmanager = new QuotaManager();
	connect(qmanager, SIGNAL(updated(QString, QString, QString, QString)),
	        this, SLOT(onQuotaUpdated(QString, QString, QString, QString)));
	connect(cmdRefresh, SIGNAL(clicked()), qmanager, SLOT(update()));
	temp_message = false;
	connect(checkBox1, SIGNAL(stateChanged(int)), this, SLOT(onCheckBox1Changed(int)));
	connect(checkBox2, SIGNAL(stateChanged(int)), this, SLOT(onCheckBox2Changed(int)));
	workPath = QDir::homePath() + "/.proxycare/";
	configPath = workPath + "proxycare.conf";
	cntlmConfigPath = workPath + "cntlm.conf";
	readConfiguration();
	
	connect(spin1, SIGNAL(valueChanged(int)), this, SLOT(onSpinValueChanged(int)));
	emit readyToConnect();
}


MainWindow::~MainWindow()
{
	delete qmanager;
	delete aes;
	foreach(IPlugin *plugin, plugins)
	{
		delete plugin;
	}
}


void MainWindow::acceptPushButtonClicked()
{
	QString user = userLineEdit->text();
	QString domain = domainLineEdit->text();
	QString proxy = proxyLineEdit->text();
	QString proxyPort = QVariant(proxyPortSpinBox->value()).toString();
	QString port = QVariant(portSpinBox->value()).toString();
	QString passw = passLineEdit->text();
	QRegExp exp;
	exp = QRegExp("^[\\w-\\.]{3,}$");

	if (!exp.exactMatch(user))
	{
		QMessageBox::warning(this, tr("Advertencia"), tr("Usuario incorrecto"), QMessageBox::Ok);
		return;
	}

	exp = QRegExp("^([\\w-]{2,}\\.)*([\\w-]{2,}\\.)[\\w-]{2,4}$");

	if (!exp.exactMatch(domain))
	{
		QMessageBox::warning(this, tr("Advertencia"), tr("Dominio incorrecto"), QMessageBox::Ok);
		return;
	}

	exp = QRegExp("^(([1-9]?[0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5]).){3}([1-9]?[0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])$");

	if (!exp.exactMatch(proxy))
	{
		QMessageBox::warning(this, tr("Advertencia"), tr("Proxy incorrecto"), QMessageBox::Ok);
		return;
	}

	exp = QRegExp("^\\d+$");

	if (!exp.exactMatch(proxyPort))
	{
		QMessageBox::warning(this, tr("Advertencia"), tr("Puerto del proxy incorrecto"), QMessageBox::Ok);
		return;
	}

	exp = QRegExp("^\\d+$");

	if (!exp.exactMatch(port))
	{
		QMessageBox::warning(this, tr("Advertencia"), tr("Puerto incorrecto"), QMessageBox::Ok);
		return;
	}

	exp = QRegExp("^\\s+$");

	if (passw.isEmpty() || exp.exactMatch(passw))
	{
		QMessageBox::warning(this, tr("Advertencia"), tr("Contraseña incorrecta"), QMessageBox::Ok);
		return;
	}

// 	QString program = cntlmPath + " -c " + cntlmConfigPath + " -fl " + port + " -p " + passw + " -u " + user + "@" + domain + " " + proxy + ":" + proxyPort;
	// QString params = " -L 443:google.com.cu:443 -N \"localhost,127.0.0.*,10.*,192.168.*,*.uci.cu ,mail.google.com,accounts.google.com,accounts.google.com.cu,fonts.gstatic.com,mail-attachment.googleusercontent.com,www.youtube.com,youtube.com,www.gmail.com,play.google.com,drive.google.com,google.co.ve,ssl.gstatic.com,s.ytimg.com,i.ytimg.com,gg.google.com,tools.google.com,apis.google.com,oauth.googleusercontent.com,www.gstatic.com,m.google.com,docs.google.com,dl.google.com,talkgadget.google.com,chatenabled.mail.google.com,plus.google.com,clients2.google.com,clients6.google.com,lh5.googleusercontent.com,images1-hangout-opensocial.googleusercontent.com,images2-hangout-opensocial.googleusercontent.com,images3-hangout-opensocial.googleusercontent.com,images4-hangout-opensocial.googleusercontent.com,maps.gstatic.com,ssl.google-analytics.com,t2.gstatic.com,groups.google.com,translate.googleusercontent.com,lh4.googleusercontent.com,webcache.googleusercontent.com,safebrowsing.google.com,www.googletagservices.com,ajax.googleapis.com,gmail.com,elian.mmafan.biz,mail.yandex.com,www.yandex.com,blacksportsonline.com,www.marca.com,www.as.com,www.google.com\"";
	QString params = "";
 	QString program = cntlmPath + params + " -fl " + port + " -p " + passw + " -u " + user + "@" + domain + " " + proxy + ":" + proxyPort;
	// std::cout << "Starting cntlm" << std::endl;
	process->start(program);

	goConnected();
}


void MainWindow::goConnected()
{
	m_connected = true;
	QString user = userLineEdit->text();
	QString password = passLineEdit->text();
	QString domain = domainLineEdit->text();
	qmanager->setUser(user);
	qmanager->setPassword(password);
	qmanager->setDomain(domain);
	qmanager->setEnabled(true);
	qmanager->update();

	connect(timer, SIGNAL(timeout()), qmanager, SLOT(update()));
	timer->start(spin1->value() * 60 * 1000);
	cmdRefresh->setEnabled(true);
}


void MainWindow::goDisconnected()
{
	m_connected = false;
	QString tt = trayIcon->toolTip().replace("Conectado.", "Desconectado.");
	trayIcon->setToolTip(tt);
}


void MainWindow::onQuotaUpdated(QString user, QString quote_used, QString quote_assigned, QString nav_level)
{
	int max = quote_assigned.toInt();
	int val = int(quote_used.toDouble());
	QString str_val = QString::number(val);
	if(val > max) val = max;
	progressBar->setMaximum(max);
	progressBar->setValue(val);
	progressBar->setToolTip(QString("Cuenta usada: %1.\nCuenta asignada: %2.").arg(str_val).arg(quote_assigned));
	this->nav_level->setText(nav_level);
	
	QString status = m_connected ? "Conectado." : "Desconectado.";
	QString text = QString(status+"\nUsuario: %1.\nCuenta: %2/%3.\n%4.").arg(user).arg(str_val).arg(quote_assigned).arg(nav_level);
	trayIcon->setToolTip(text);
}


void MainWindow::setVisible(bool visible)
{
	minimizeAction->setEnabled(visible);
	restoreAction->setEnabled(isMaximized() || !visible);
	QDialog::setVisible(visible);
}


void MainWindow::closeEvent(QCloseEvent *event)
{
	if (trayIcon->isVisible() && m_connected)
	{
		hide();
		event->ignore();
		QString message = "ProxyCare se encuentra \nactualmente activo en segundo plano.\nPara cerrar escoja quitar en el menu\ncontextual.";
		showMessage(tr("Aviso"), message, 5000);
	}
	else
	{
		quit();
	}
}


void MainWindow::iconActivated(QSystemTrayIcon::ActivationReason reason)
{
	switch (reason)
	{
		case QSystemTrayIcon::Trigger:
		case QSystemTrayIcon::DoubleClick:
			this->showNormal();
			break;
		case QSystemTrayIcon::MiddleClick:
			changeConnectionStatus();
			break;
		default:
			;
	}
}


void MainWindow::changeConnectionStatus()
{
	if(m_connected) terminateProcess();
	else acceptPushButtonClicked();
}


void MainWindow::showMessage(QString title, QString message, int time)
{
	if (!temp_message && !checkBox2->isChecked())
	{
		QSystemTrayIcon::MessageIcon icon = QSystemTrayIcon::MessageIcon(QSystemTrayIcon::Information);
		trayIcon->showMessage(title, message, icon, time);
		temp_message = true;
	}
}


void MainWindow::messageClicked()
{
}


void MainWindow::createActions()
{
	minimizeAction = new QAction(tr("Mi&nimizar"), this);
	connect(minimizeAction, SIGNAL(triggered()), this, SLOT(hide()));

	restoreAction = new QAction(tr("&Restaurar"), this);
	connect(restoreAction, SIGNAL(triggered()), this, SLOT(showNormal()));

	quitAction = new QAction(tr("&Quitar"), this);
	connect(quitAction, SIGNAL(triggered()), this, SLOT(quit()));
}


void MainWindow::quit()
{
	saveConfiguration();
	terminateProcess();
	qApp->quit();
}


void MainWindow::hide()
{
	QDialog::hide();
}


void MainWindow::createTrayIcon()
{
	trayIconMenu = new QMenu(this);
	trayIconMenu->addAction(minimizeAction);
	trayIconMenu->addAction(restoreAction);
	trayIconMenu->addSeparator();
	trayIconMenu->addAction(quitAction);

	trayIcon = new QSystemTrayIcon(this);
	QByteArray category = qgetenv("SNI_CATEGORY");

	if (!category.isEmpty())
	{
		trayIcon->setProperty("_qt_sni_category", QString::fromLocal8Bit(category));
	}

	trayIcon->setIcon(QIcon(":/images/logo-off.svg"));
	trayIcon->setContextMenu(trayIconMenu);
	trayIcon->setToolTip(QApplication::translate("Dialog", "Desconectado"));

	trayIcon->installEventFilter(this);
}


void MainWindow::readConfiguration()
{
	QByteArray data = decrypt(configPath);
	QDomDocument doc;

	if (!doc.setContent(data, false))
	{
		quit();
		return;
	}

	QDomElement docElem = doc.documentElement();
	QDomNodeList nodeList = docElem.elementsByTagName("Option");

	for (int i = 0; i < nodeList.length(); i++)
	{
		QDomElement element = nodeList.at(i).toElement();

		if (element.attribute("name") == "user")
			userLineEdit->setText(element.attribute("value"));
		else if (element.attribute("name") == "password")
		{
			passLineEdit->setText(element.attribute("value"));
			savePassCheckBox->setChecked(true);
		}
		else if (element.attribute("name") == "domain")
			domainLineEdit->setText(element.attribute("value"));
		else if (element.attribute("name") == "proxy")
			proxyLineEdit->setText(element.attribute("value"));
		else if (element.attribute("name") == "proxyPort")
			proxyPortSpinBox->setValue(element.attribute("value").toInt());
		else if (element.attribute("name") == "port")
			portSpinBox->setValue(element.attribute("value").toInt());

		else if (element.attribute("name") == "interval")
			spin1->setValue(element.attribute("value").toInt());

		else if (element.attribute("name") == "start")
			checkBox1->setChecked(QVariant(element.attribute("value")).toBool());
		else if (element.attribute("name") == "hidden")
			checkBox2->setChecked(QVariant(element.attribute("value")).toBool());
	}

	if (checkBox1->isChecked()) // auto connect
	{
		acceptPushButtonClicked();
	}

	if (checkBox2->isChecked()) // hide on start
	{
		QTimer::singleShot(0, this, SLOT(hide()));
		hide();
	}
}


void MainWindow::saveConfiguration()
{
	QString user = userLineEdit->text();
	QString password = passLineEdit->text();
	QString domain = domainLineEdit->text();
	QString proxy = proxyLineEdit->text();
	QString proxyPort = QVariant(proxyPortSpinBox->value()).toString();
	QString port = QVariant(portSpinBox->value()).toString();
	QDomDocument domDocument;
	domDocument.appendChild(domDocument.createProcessingInstruction("xml", "version=\"1.0\" encoding=\"UTF-8\""));
	QDomElement rootNode = domDocument.createElement("Configuration");

	QDomElement node = domDocument.createElement("Option");
	node.setAttribute("name", "user");
	node.setAttribute("value", user);
	rootNode.appendChild(node);

	node = domDocument.createElement("Option");
	node.setAttribute("name", "domain");
	node.setAttribute("value", domain);
	rootNode.appendChild(node);

	node = domDocument.createElement("Option");
	node.setAttribute("name", "proxy");
	node.setAttribute("value", proxy);
	rootNode.appendChild(node);


	node = domDocument.createElement("Option");
	node.setAttribute("name", "proxyPort");
	node.setAttribute("value", proxyPort);
	rootNode.appendChild(node);

	node = domDocument.createElement("Option");
	node.setAttribute("name", "port");
	node.setAttribute("value", port);
	rootNode.appendChild(node);

	node = domDocument.createElement("Option");
	node.setAttribute("name", "interval");
	node.setAttribute("value", QVariant(spin1->value()).toString());
	rootNode.appendChild(node);

	node = domDocument.createElement("Option");
	node.setAttribute("name", "start");
	node.setAttribute("value", QVariant(checkBox1->isChecked()).toString());
	rootNode.appendChild(node);

	node = domDocument.createElement("Option");
	node.setAttribute("name", "hidden");
	node.setAttribute("value", QVariant(checkBox2->isChecked()).toString());
	rootNode.appendChild(node);


	if (savePassCheckBox->isChecked())
	{
		node = domDocument.createElement("Option");
		node.setAttribute("name", "password");
		node.setAttribute("value", password);
		rootNode.appendChild(node);
	}


	domDocument.appendChild(rootNode);

	QDir workDir(workPath);

	if (!workDir.exists())
		workDir.mkpath(workPath);

	encrypt(configPath, domDocument.toByteArray());

}


void MainWindow::standardOutput()
{
	QString out = process->readAllStandardOutput();
	QRegExp delRegExp("\\s*cntlm\\[\\d+\\]\\:\\s*");
	out.remove(delRegExp);
	// qDebug() << out;
}


void MainWindow::standardError()
{
	QString out = process->readAllStandardError();
	QRegExp exp( "^.*Exitting with error.*$" );

	if (exp.exactMatch(out))
	{
		QString message = "No se ha podido establecer la conexión,\nverifique que cntlm no esté actualmente en ejecución\ny que los parámetros estén correctos";
		QMessageBox::warning(this, tr("Error"), message, QMessageBox::Ok);
		this->showNormal();
	}

	QRegExp delRegExp("\\s*cntlm\\[\\d+\\]\\:\\s*");
	out = out.remove(delRegExp);
	// qDebug() << out;
}


void MainWindow::all()
{
	QString out = process->readAll();
	QRegExp delRegExp("\\s*cntlm\\[\\d+\\]\\:\\s*");
	out.remove(delRegExp);
	// qDebug() << out;
}


void MainWindow::processFinished()
{
	std::cout << "Cntlm is now closed" << std::endl;
	trayIcon->setIcon(QIcon(":/images/logo-off.svg"));
	emit readyToConnect();
}


void MainWindow::terminateProcess()
{
	if (process->state() == QProcess::Running)
	{
		std::cout << "Closing cntlm" << std::endl;
		process->execute("killall -s KILL cntlm");
	}

	emit readyToConnect();
	goDisconnected();
}


void MainWindow::processStarted()
{
	trayIcon->setIcon(QIcon(":/images/logo-on.svg"));
	emit connected();
}


void MainWindow::processError(QProcess::ProcessError error)
{
	if (error == QProcess::FailedToStart)
	{
		std::cout << "Failed to start cntlm process" << std::endl;
		QString message = "No se ha podido establecer la conexión,\nno se encuentra " + cntlmPath;
		QMessageBox::warning(this, tr("Error"), message, QMessageBox::Ok);
		emit readyToConnect();
		this->showNormal();
	}

	goDisconnected();
}


void MainWindow::uiReadyToConnect()
{
	frame1->setEnabled(true);
	frame2->setEnabled(true);
	spin1->setEnabled(true);
	acceptPushButton->setEnabled(true);
	cancellPushButton->setEnabled(false);
}


void MainWindow::uiConnected()
{
	frame1->setEnabled(false);
	frame2->setEnabled(false);
// 	spin1->setEnabled(false);
	acceptPushButton->setEnabled(false);
	cancellPushButton->setEnabled(true);
}


void MainWindow::onCheckBox1Changed(int)
{
}


void MainWindow::onCheckBox2Changed(int)
{

}


void MainWindow::encrypt(QString fileName, QByteArray data)
{
	QByteArray encrypted = aes->Encrypt(data, ppassword);

	QFile file(fileName);

	if (!file.open(QIODevice::WriteOnly))
	{
		quit();
	}

	file.write(encrypted);
	file.close();
}


QByteArray MainWindow::decrypt(QString fileName)
{
	QFile file(fileName);

	if (!file.open(QIODevice::ReadOnly))
	{
		quit();
	}

	QByteArray input = file.readAll();
	file.close();

	QByteArray decrypted = aes->Decrypt(input, ppassword);

	return decrypted;
}


int MainWindow::loadPlugins()
{
 	QString str_plugins_dir = "/usr/lib/proxycare/plugins/";
	int count = 0;
	QDir pluginsDir(str_plugins_dir);
	// qDebug() << str_plugins_dir;
		
	foreach (QString fileName, pluginsDir.entryList(QDir::Files)) 
	{
		// qDebug() << fileName;
        QPluginLoader loader(pluginsDir.absoluteFilePath(fileName));
        QObject *plugin = loader.instance();
        if (plugin) 
		{
            populateTabs(plugin, &count);
        }
    }
	
	return count;
}


void MainWindow::unloadPlugin(IPlugin* /*plugin*/)
{
}


void MainWindow::populateTabs(QObject *obj, int *count)
{
	IPlugin *plugin = qobject_cast<IPlugin *>(obj);
    if (plugin)
	{
		// qDebug() << "New plugin founded.";
		QString p_name = plugin->name();
		QWidget *p_widget = plugin->widget();
		QString p_tabToolTip = plugin->tabToolTip();
		QIcon p_tabIcon = plugin->tabIcon();
		if(p_name == NULL || p_name.isNull() || p_name.isEmpty())
		{
			delete plugin;
			return;
		}
		if(p_widget == NULL)
		{
			delete plugin;
			return;
		}
		if(p_tabToolTip == NULL || p_tabToolTip.isNull() || p_tabToolTip.isEmpty())
		{
			delete plugin;
			return;
		}
		if(p_tabIcon.isNull())
		{
			delete plugin;
			return;
		}
		
		plugins.append(plugin);
		
        QWidget *tab = new QWidget();
		tab->setObjectName(QString("plugin%1").arg(*count++));
		
		QVBoxLayout *tabLayout = new QVBoxLayout(tab);
		tabLayout->addWidget(p_widget);
		plugin->widget()->show();
		
        tabWidget->addTab(tab, p_tabIcon, QString());
		
		tabWidget->setTabText(tabWidget->indexOf(tab), QString());
        tabWidget->setTabToolTip(tabWidget->indexOf(tab), p_tabToolTip);
	}
}


void MainWindow::onSpinValueChanged(int value)
{
	timer->setInterval(value * 60 * 1000);
}
